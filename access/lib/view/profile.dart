// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  String _jk = "";

  TextEditingController controllerUsername = new TextEditingController();
  TextEditingController controllerEmail = new TextEditingController();
  TextEditingController controllerTelepon = new TextEditingController();
  TextEditingController controllerTanggalLahir = new TextEditingController();

  void _pilihJk(String value) {
    setState(() {
      _jk = value;
    });
  }

  void kirimdata() {
    AlertDialog alertDialog = new AlertDialog(
      content: new Container(
        height: 200.0,
        child: new Column(
          children: <Widget>[
            new Padding(
                  padding: new EdgeInsets.only(top: 20.0),
                ),
            new Text("Username : ${controllerUsername.text}", 
            style: TextStyle(fontFamily: 'Poppins'),),
            new Text("Email : ${controllerEmail.text}", 
            style: TextStyle(fontFamily: 'Poppins'),),
            new Text("Telepon : ${controllerTelepon.text}", 
            style: TextStyle(fontFamily: 'Poppins'),),
            new Text("Tanggal Lahir : ${controllerTanggalLahir.text}", 
            style: TextStyle(fontFamily: 'Poppins'),),
            new Text("Jenis Kelamin : ${_jk}", 
            style: TextStyle(fontFamily: 'Poppins'),),
            new Padding(
                  padding: new EdgeInsets.only(top: 20.0),
                ),
            new RaisedButton(
              child: new Text("Oke", 
              style: TextStyle(fontFamily: 'Poppins'),),
              onPressed: () => Navigator.pop(context),
              color: Color.fromARGB(255, 145, 79, 156),
            )
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }

  final _formKey = GlobalKey<FormState>(); 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: profileView(),
        );
  }

  Widget profileView() {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(25),
            child: Text(
              'Profile details',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, fontFamily: 'Poppins',),
            ),
          ),
          
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 50),
            child: Stack(
              children: <Widget>[
                CircleAvatar(
                  radius: 60,
                  child: ClipOval(
                      child: Image.asset(
                    'images/profile.jpg',
                    height: 150,
                    width: 150,
                    fit: BoxFit.cover,
                  )),
                ),
                Positioned(
                  bottom: 1,
                  right: 10,
                  child: Container(
                    height: 25,
                    width: 25,
                    child: Icon(
                      Icons.add_a_photo,
                      color: Colors.black,
                      size: 15,
                    ),
                    decoration: BoxDecoration(
                        color: Color.fromARGB(255, 145, 79, 156),
                        borderRadius: BorderRadius.all(Radius.circular(25))),
                  ),
                ),
              ],
            ),
          ),
    
          new Container(
            padding: const EdgeInsets.all(10.0),
            child: Form(
              key: _formKey,
              child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Column(children: <Widget>[
                  TextFormField(
                    controller: controllerUsername,
                    cursorColor: Color.fromARGB(255, 145, 79, 156),
                    decoration: new InputDecoration(
                      hintText: "masukkan nama anda",
                      hintStyle: TextStyle(
                        color: Colors.black54,
                        fontFamily: 'Poppins',
                      ),
                      labelText: "Username",
                      labelStyle: TextStyle(
                        color: Color.fromARGB(255, 145, 79, 156),
                        fontFamily: 'Poppins',
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromARGB(255, 145, 79, 156),
                        ),
                        borderRadius: new BorderRadius.circular(20.0),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromARGB(255, 145, 79, 156),
                        ),
                        borderRadius: new BorderRadius.circular(20.0),
                      ),
                    ),
                    validator: (value) {
                        if (value!.isEmpty) {
                          return 'username tidak boleh kosong';
                        }
                        return null;
                      },
                  ),
                  new Padding(
                    padding: new EdgeInsets.only(top: 20.0),
                  ),
    
    
                  TextFormField(
                    controller: controllerEmail,
                    cursorColor: Color.fromARGB(255, 145, 79, 156),
                    decoration: new InputDecoration(
                      hintText: "masukkan email anda",
                      hintStyle: TextStyle(
                        color: Colors.black54,
                        fontFamily: 'Poppins',
                      ),
                      labelText: "Email",
                      labelStyle: TextStyle(
                        color: Color.fromARGB(255, 145, 79, 156),
                        fontFamily: 'Poppins',
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromARGB(255, 145, 79, 156),
                        ),
                        borderRadius: new BorderRadius.circular(20.0),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromARGB(255, 145, 79, 156),
                        ),
                        borderRadius: new BorderRadius.circular(20.0),
                      ),
                    ),
                    validator: (value) {
                        if (value!.isEmpty) {
                          return 'email tidak boleh kosong';
                        }
                        return null;
                      },
                  ),
                  new Padding(
                    padding: new EdgeInsets.only(top: 20.0),
                  ),
    
    
                  TextFormField(
                    controller: controllerTelepon,
                    cursorColor: Color.fromARGB(255, 145, 79, 156),
                    decoration: new InputDecoration(
                      hintText: "masukkan no. hp anda",
                      hintStyle: TextStyle(
                        color: Colors.black54,
                        fontFamily: 'Poppins',
                      ),
                      labelText: "Telepon",
                      labelStyle: TextStyle(
                        color: Color.fromARGB(255, 145, 79, 156),
                        fontFamily: 'Poppins',
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromARGB(255, 145, 79, 156),
                        ),
                        borderRadius: new BorderRadius.circular(20.0),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromARGB(255, 145, 79, 156),
                        ),
                        borderRadius: new BorderRadius.circular(20.0),
                      ),
                    ),
                    validator: (value) {
                        if (value!.isEmpty) {
                          return 'no. hp tidak boleh kosong';
                        }
                        return null;
                      },
                  ),
                  new Padding(
                    padding: new EdgeInsets.only(top: 20.0),
                  ),
    
                  TextFormField(
                    controller: controllerTanggalLahir,
                    cursorColor: Color.fromARGB(255, 145, 79, 156),
                    decoration: new InputDecoration(
                      hintText: "masukkan tanggal lahir anda",
                      hintStyle: TextStyle(
                        color: Colors.black54,
                        fontFamily: 'Poppins',
                      ),
                      labelText: "Tanggal Lahir",
                      labelStyle: TextStyle(
                        color: Color.fromARGB(255, 145, 79, 156),
                        fontFamily: 'Poppins',
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromARGB(255, 145, 79, 156),
                        ),
                        borderRadius: new BorderRadius.circular(20.0),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromARGB(255, 145, 79, 156),
                        ),
                        borderRadius: new BorderRadius.circular(20.0),
                      ),
                    ),
                    validator: (value) {
                        if (value!.isEmpty) {
                          return 'tangal lahir tidak boleh kosong';
                        }
                        return null;
                      },
                  ),
                  new Padding(
                    padding: new EdgeInsets.only(top: 20.0),
                  ),
    
                  new RadioListTile(
                      value: "laki-laki",
                      title: new Text("Laki-laki",
                      style: TextStyle(fontFamily: 'Poppins'),),
                      groupValue: _jk,
                      onChanged: (String? value) {
                        _pilihJk(value!);
                      },
                      activeColor: Colors.blue,
                      subtitle: new Text("Pilih ini jika anda laki-laki",
                      style: TextStyle(fontFamily: 'Poppins'),),
                    ),
                    new RadioListTile(
                      value: "perempuan",
                      title: new Text("Perempuan",
                      style: TextStyle(fontFamily: 'Poppins'),),
                      groupValue: _jk,
                      onChanged: (String? value) {
                        _pilihJk(value!);
                      },
                      activeColor: Colors.red,
                      subtitle: new Text("Pilih ini jika anda perempuan", 
                      style: TextStyle(fontFamily: 'Poppins'),),
                    ),
                    new Padding(
                    padding: new EdgeInsets.only(top: 40.0),
                  ),
    
                  new RaisedButton(
                        child: new Text("Save", 
                        style: TextStyle(fontFamily: 'Poppins'),),
                        color: Color.fromARGB(255, 145, 79, 156),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            kirimdata();
                          }
                        })
                ]
                )
              ],
            ),
            ),
          ),
        ],
      ),
    );
  }
}