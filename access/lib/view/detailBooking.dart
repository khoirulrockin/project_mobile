import 'package:flutter/material.dart';

class rincianTransaksi extends StatefulWidget {
  @override
  State<rincianTransaksi> createState() => _rincianTransaksiState();
}

class _rincianTransaksiState extends State<rincianTransaksi> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail Booking'),
      ),
      body: Container(        
        margin: EdgeInsets.all(5),
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.indigo,
        ),
        child:Column(
              //mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const SizedBox(
                  height: 20,
                ),
                Text("Kode Booking : K5GU8OM",
                  style: TextStyle(
                    fontSize: 31,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Roboto',
                  ),
                  textAlign: TextAlign.left,                  
                ),
                Text("Nama Lengkap : ",
                  style: TextStyle(
                    fontSize: 25,
                  ),
                  textAlign: TextAlign.left,
                ),
                Text("Nomor Hp : ",
                  style: TextStyle(
                    fontSize: 25,
                  ),
                ),
                Text("Alamat : ",
                  style: TextStyle(
                    fontSize: 25,
                  ),
                  maxLines: 2,
                ),
                Text("Nama Studio : ",
                  style: TextStyle(
                    fontSize: 25,
                  ),
                ),
                Text("Jam Sewa : ",
                  style: TextStyle(
                    fontSize: 25,
                  ),
                ),
                Text("Tanggal Sewa : ",
                  style: TextStyle(
                    fontSize: 25,
                  ),
                ),
                Text("Paket : ",
                  style: TextStyle(
                    fontSize: 25,
                  ),
                ),
              ],
            ),
      ),
    );
  }
}
