import 'package:flutter/material.dart';

class rincianStudio extends StatefulWidget {
  @override
  State<rincianStudio> createState() => _rincianStudioState();
}

class _rincianStudioState extends State<rincianStudio>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: rincianStudioView(),
        );
  }

  Widget rincianStudioView() {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
            child: Stack(
              children: <Widget>[
                Image(image: NetworkImage('https://picsum.photos/seed/251/600'),),
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                Text('Pesona Studio Foto',
                style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.bold, fontSize: 20),),
                Text('Jln. Kalimantan no.100, Jember',
                style: TextStyle(fontFamily: 'Poppins', fontSize: 12, color: Colors.blueGrey),),
                Text('No. Telepon : 082132816428',
                style: TextStyle(fontFamily: 'Poppins', fontSize: 12, color: Colors.blueGrey),)
              ],
            ),
            // child: Text(
            //   'Pesona Studio Foto',
            //   style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, fontFamily: 'Poppins',),),
              
          ),
          
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Pesona studio foto merupakan sebuah studio foto yang terletak di jalan kaliamantan no.100, dimana kami menyediakan berbagai ragam background untuk foto keluarga, graduation, foto grup, prewedding maupun yang lain dengan berbagai properti yang menunjang kebutuhan foto. Selain itu, kami juga memilik lahan parkir yang luas, tempat foto yang bisa menampung 30 orang serta dengan tema yang sangat bervariasi. Kami juga menyediakan jasa foto untuk diluar studio dengan harga yang terjangkau. Berikut ini paket yang kami tawarkan :', style: TextStyle(fontFamily: 'Poppins',),),
                  Text('', style: TextStyle(fontFamily: 'Poppins'),),
                  Text('Paket family : Rp. 150.000', style: TextStyle(fontFamily: 'Poppins'),),
                  Text('Paket graduation : Rp. 160.00', style: TextStyle(fontFamily: 'Poppins'),),
                  Text('Paket groups : Rp. 100.00', style: TextStyle(fontFamily: 'Poppins'),),
                  Text('Paket prewedding : Rp. 200.000', style: TextStyle(fontFamily: 'Poppins'),),
                  Text('', style: TextStyle(fontFamily: 'Poppins'),),
                  Text('Dengan ketentuan yang sama di tiap paketnya yaitu dapat foto sepuasnya dalam jangka waktu 20 menit, mendapat seluruh softfile dengan 5 foto pilihan untuk diedit', style: TextStyle(fontFamily: 'Poppins'),),
                  const SizedBox(height: 12),
                ]
              ),
          ),


          // Padding(
          //   padding: const EdgeInsets.all(10.0),
          //   child: Card(
          //     color: Colors.lightBlue,
          //     elevation: 8,
          //     shape: RoundedRectangleBorder(
          //       borderRadius: BorderRadius.circular(12),
          //     ),
          //     child: Column(
          //       crossAxisAlignment: CrossAxisAlignment.start,
          //       children: [
          //         const SizedBox(height: 12),
          //         Text(
          //           'Paket B',
          //           style: TextStyle(fontWeight: FontWeight.bold),
          //         ),
          //         Text(
          //           'Studio foto Paket B merupakan studio foto khusus untuk foto Graduation, fasilitas di dalamnya meliputi : Pencahayaan, Properti, Background, Baju Kelulusan dan lain-lain. Harga sewa untuk Studio foto paket B sebesar Rp. 100.000/15 foto ',
          //           style: TextStyle(fontFamily: 'Poppins'),
          //         ),
          //         const SizedBox(height: 12),
          //       ]
          //     ),
          //   )
          // ),

          // Padding(
          //   padding: const EdgeInsets.all(10.0),
          //   child: Card(
          //     color: Colors.lightBlue,
          //     elevation: 8,
          //     shape: RoundedRectangleBorder(
          //       borderRadius: BorderRadius.circular(12),
          //     ),
          //     child: Column(
          //       crossAxisAlignment: CrossAxisAlignment.start,
          //       children: [
          //         const SizedBox(height: 12),
          //         Text(
          //           'Paket C',
          //           style: TextStyle(fontWeight: FontWeight.bold),
          //         ),
          //         Text(
          //           'Studio foto Paket C merupakan studio foto khusus untuk foto Group bersama teman, fasilitas di dalamnya meliputi : Pencahayaan, Properti, Background, Baju bertema tertentu yang bisa di request dan lain-lain. Harga sewa untuk Studio foto paket C sebesar Rp. 100.000/15 foto ',
          //           style: TextStyle(fontFamily: 'Poppins'),
          //         ),
          //         const SizedBox(height: 12,),
          //       ]
          //     ),
          //   )
          // ),
        ],
      ),
    );
  }
}
