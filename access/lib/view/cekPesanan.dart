import 'package:flutter/material.dart';

class CekPesananPage extends StatefulWidget {
  const CekPesananPage({Key? key}) : super(key: key);

  @override
  State<CekPesananPage> createState() => _CekPesananPageState();
}

class _CekPesananPageState extends State<CekPesananPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView.builder(
      itemCount: 15,
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.only(top: 5),
          child: Container(
              margin: EdgeInsets.zero,
              
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Colors.cyan,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Kode Booking: K3IR6I",
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Roboto'),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "Nama Studio:Seven Studio",
                        style: TextStyle(fontSize: 15),
                      ),
                      SizedBox(width: 5),
                      Text(
                        "Tanggal Booking:1/09/2019",
                        style: TextStyle(fontSize: 15),
                      ),
                      SizedBox(width: 5),
                      Text(
                        "Jam Booking: 13.00",
                        style: TextStyle(fontSize: 15),
                      ),
                      SizedBox(width: 5),
                    ],
                  ),
                ],
              ),
              ),
        );
      },
    ),
    );
  }
}
