// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';

class PesananPage extends StatefulWidget {
  const PesananPage({Key? key}) : super(key: key);

  @override
  _PesananPageState createState() => _PesananPageState();
}

class _PesananPageState extends State<PesananPage> {
  List<String> listPaket = ["Paket A", "Paket B", "Paket C", "Paket D"];

  String nPaket = "Paket A";
  int? nilaiPaket;

  TextEditingController controllerNamaLengkap = new TextEditingController();
  TextEditingController controllerTelepon = new TextEditingController();
  TextEditingController controllerAlamat = new TextEditingController();
  TextEditingController controllerNamaStudio = new TextEditingController();
  TextEditingController controllerJamSewa = new TextEditingController();
  TextEditingController controllerTanggalPakai = new TextEditingController();

  void pilihPaket(String value) {
    //menampilkan Paket yang dipilih
    setState(() {
      nPaket = value;
    });
  }

  void kirimdata() {
    AlertDialog alertDialog = new AlertDialog(
      content: new Container(
        height: 200.0,
        child: new Column(
          children: <Widget>[
            new Padding(
                  padding: new EdgeInsets.only(top: 60.0),
                ),
            new Text("Studio Berhasil Dipesan"),
            // new Text("Nama Lengkap : ${controllerNamaLengkap.text}"),
            // new Text("Alamat : ${controllerAlamat.text}"),
            // new Text("Telepon : ${controllerTelepon.text}"),
            // new Text("Studio : ${controllerNamaStudio.text}"),
            // new Text("Jam Sewa : ${controllerJamSewa.text}"),
            // new Text("Tanggal Pakai : ${controllerTanggalPakai.text}"),
            // new Text("Paket : ${nPaket}"),
            new Padding(
                  padding: new EdgeInsets.only(top: 60.0),
                ),
            new RaisedButton(
              child: new Text("Oke"),
              onPressed: () => Navigator.pop(context),
              color: Colors.blue,
            )
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pemesanan'),
      ),
      body: SingleChildScrollView(
        child: Card(
          color: Colors.blueAccent.withOpacity(0.35),
          elevation: 50,
          shape: OutlineInputBorder(
              borderRadius: BorderRadius.circular(50),
              borderSide: BorderSide(color: Colors.white, width: 2)),
          margin: const EdgeInsets.only(bottom: 13),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(25, 13, 25, 25),
            child: Column(children: <Widget>[
              const SizedBox(
                height: 30,
              ),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 13),
                child: Form(
                  key: _formKey,
                  child: Column(
                  children: <Widget>[
                    Material(
                      color: Colors.blue,
                      borderRadius: const BorderRadius.all(Radius.circular(20)),
                      elevation: 2,
                      child: AspectRatio(
                        aspectRatio: 13 / 1.3,
                        child: Center(
                          child: TextFormField(
                            controller: controllerNamaLengkap,
                              decoration: const InputDecoration(
                            hintText: 'Nama Lengkap',
                            hintStyle:
                                TextStyle(fontFamily: 'Poppins', fontSize: 16),
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.all(13),
                            //keyboardType: ,
                            //controller: ,
                          )),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 16),
                      child: Material(
                        color: Colors.blue,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(20)),
                        elevation: 2,
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                child: AspectRatio(
                              aspectRatio: 13 / 1.3,
                              child: Center(
                                child: TextFormField(
                                  controller: controllerAlamat,
                                  decoration: const InputDecoration(
                                    hintText: 'Alamat',
                                    hintStyle: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 16,
                                    ),
                                    border: InputBorder.none,
                                    contentPadding: EdgeInsets.all(13),
                                    //controller
                                  ),
                                ),
                              ),
                            )),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 16),
                      child: Material(
                        color: Colors.blue,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(20)),
                        elevation: 2,
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                child: AspectRatio(
                              aspectRatio: 13 / 1.3,
                              child: Center(
                                child: TextFormField(
                                  controller: controllerTelepon,
                                  decoration: const InputDecoration(
                                    hintText: '+62981365432130',
                                    hintStyle: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 16,
                                    ),
                                    border: InputBorder.none,
                                    contentPadding: EdgeInsets.all(13),
                                    //controller
                                  ),
                                ),
                              ),
                            )),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 16),
                      child: Material(
                        color: Colors.blue,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(20)),
                        elevation: 2,
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                child: AspectRatio(
                              aspectRatio: 13 / 1.3,
                              child: Center(
                                child: TextFormField(
                                  controller: controllerNamaStudio,
                                  decoration: const InputDecoration(
                                    hintText: 'Nama Studio',
                                    hintStyle: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 16,
                                    ),
                                    border: InputBorder.none,
                                    contentPadding: EdgeInsets.all(13),
                                    //controller
                                  ),
                                ),
                              ),
                            )),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 16),
                      child: Material(
                        color: Colors.blue,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(20)),
                        elevation: 2,
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                child: AspectRatio(
                              aspectRatio: 13 / 1.3,
                              child: Center(
                                child: TextFormField(
                                  controller: controllerJamSewa,
                                  decoration: const InputDecoration(
                                    hintText: 'Jam Sewa',
                                    hintStyle: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 16,
                                    ),
                                    border: InputBorder.none,
                                    contentPadding: EdgeInsets.all(13),
                                    //controller
                                  ),
                                ),
                              ),
                            )),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 16),
                      child: Material(
                        color: Colors.blue,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(20)),
                        elevation: 2,
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                child: AspectRatio(
                              aspectRatio: 13 / 1.3,
                              child: Center(
                                child: TextFormField(
                                  controller: controllerTanggalPakai,
                                  decoration: const InputDecoration(
                                    hintText: 'dd/mm/yyyy',
                                    hintStyle: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 16,
                                    ),
                                    border: InputBorder.none,
                                    contentPadding: EdgeInsets.all(13),
                                    //controller
                                  ),
                                ),
                              ),
                            )),
                          ],
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text('Pilih Paket : '),
                        SizedBox(width: 20),
                        DropdownButton(
                          value: nPaket,
                          onChanged: (String? value) {
                            pilihPaket(value ?? "");
                            nilaiPaket = listPaket.indexOf(value ?? "");
                          },
                          items: listPaket.map((String value) {
                            return DropdownMenuItem(
                              child: Text(value),
                              value: value,
                            );
                          }).toList(),
                        ),
                      ],
                    ),

                    new Padding(
                    padding: new EdgeInsets.only(top: 30.0),
                    ),
                    new RaisedButton(
                      child: new Text("Pesan"),
                      color: Colors.blue,
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          kirimdata();
                        }
                      })
                  ],
                ),
                )
              ),
            ]),
          ),
        ),
      ),
    );
  }
}