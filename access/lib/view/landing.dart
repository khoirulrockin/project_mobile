import 'package:flutter/material.dart';
import 'home.dart';
import 'cekPesanan.dart';
import 'pengaturan.dart';
import 'pesanan.dart';
import 'package:flutter_project/constan.dart';
// import 'package:bs_access/constan.dart';

class LandingPage extends StatefulWidget {
  @override
  State<LandingPage> createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  int _bottomNavCurrentIndex = 0;
  List<Widget> _container = [
    new HomePage(),
    new PesananPage(),
    new CekPesananPage(),
    new PengaturanPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _container[_bottomNavCurrentIndex],
        bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: Palette.bg1,
          type: BottomNavigationBarType.fixed,
          onTap: (index) {
            setState(() {
              _bottomNavCurrentIndex = index;
            });
          },
          currentIndex: _bottomNavCurrentIndex,
          items: [
            BottomNavigationBarItem(
              activeIcon: new Icon(
                Icons.home,
                color: Palette.bg1,
              ),
              icon: new Icon(
                Icons.home,
                color: Colors.grey,
              ),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              activeIcon: new Icon(
                Icons.attach_money,
                color: Palette.bg1,
              ),
              icon: new Icon(
                Icons.attach_money,
                color: Colors.grey,
              ),
              label: 'Transaksi', 
            ),
            BottomNavigationBarItem(
              activeIcon: new Icon(
                Icons.payments_rounded,
                color: Palette.bg1,
              ),
              icon: new Icon(
                Icons.payments_rounded,
                color: Colors.grey,
              ),
              label: 'Booking', 
            ),
            BottomNavigationBarItem(
              activeIcon: new Icon(
                Icons.settings,
                color: Palette.bg1,
              ),
              icon: new Icon(
                Icons.settings,
                color: Colors.grey,
              ),
              label: 'Profile', 
            ),
          ],
        ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
