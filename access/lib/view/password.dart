
// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';

class Password extends StatefulWidget {
  @override
  State<Password> createState() => _PasswordState();
}

class _PasswordState extends State<Password> {

  TextEditingController controllerPassLama = new TextEditingController();
  TextEditingController controllerPassBaru = new TextEditingController();
  TextEditingController controllerKonfirmasi = new TextEditingController();

  void kirimdata() {
    AlertDialog alertDialog = new AlertDialog(
      content: new Container(
        height: 200.0,
        child: new Column(
          children: <Widget>[
            new Padding(
                  padding: new EdgeInsets.only(top: 60.0),
                ),
            new Text("Kata Sandi Berhasil Diperbarui", 
            style: TextStyle(fontFamily: 'Poppins'),),
            // new Text("Kata Sandi Lama : ${controllerPassLama.text}"),
            // new Padding(
            //       padding: new EdgeInsets.only(top: 20.0),
            //     ),
            // new Text("Kata Sandi Baru : ${controllerPassBaru.text}"),
            // new Padding(
            //       padding: new EdgeInsets.only(top: 20.0),
            //     ),
            // new Text("Konfirmasi Kata Sandi : ${controllerKonfirmasi.text}"),
            new Padding(
                  padding: new EdgeInsets.only(top: 50.0),
                ),
            new RaisedButton(
              child: new Text("Oke",
              style: TextStyle(fontFamily: 'Poppins'),),
              onPressed: () => Navigator.pop(context),
              color: Color.fromARGB(255, 145, 79, 156),
            )
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }

  final _formKey = GlobalKey<FormState>(); 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: passwordView(),
        );
  }

  Widget passwordView() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(25),
          child: Text(
            'Ganti Password',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, fontFamily: 'Poppins',),
          ),
        ),
        
        new Container(
          padding: const EdgeInsets.all(10.0),
          child: Form(
            key: _formKey,
            child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(children: <Widget>[
                TextFormField(
                  controller: controllerPassLama,
                  obscureText: true,
                  cursorColor: Color.fromARGB(255, 145, 79, 156),
                  decoration: new InputDecoration(
                    hintText: "masukkan kata sandi lama",
                    hintStyle: TextStyle(
                      color: Colors.black54,
                      fontFamily: 'Poppins',
                    ),
                    labelText: "Kata Sandi Lama",
                    labelStyle: TextStyle(
                      color: Color.fromARGB(255, 145, 79, 156),
                      fontFamily: 'Poppins',
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromARGB(255, 145, 79, 156),
                      ),
                      borderRadius: new BorderRadius.circular(20.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromARGB(255, 145, 79, 156),
                      ),
                      borderRadius: new BorderRadius.circular(20.0),
                    ),
                  ),
                  validator: (value) {
                      if (value!.isEmpty) {
                        return 'kata sandi harus diisi';
                      }
                      return null;
                    },
                ),
                new Padding(
                  padding: new EdgeInsets.only(top: 20.0),
                ),


                TextFormField(
                  controller: controllerPassBaru,
                  obscureText: true,
                  cursorColor: Color.fromARGB(255, 145, 79, 156),
                  decoration: new InputDecoration(
                    hintText: "masukkan kata sandi baru",
                    hintStyle: TextStyle(
                      color: Colors.black54,
                      fontFamily: 'Poppins',
                    ),
                    labelText: "Kata Sandi Baru",
                    labelStyle: TextStyle(
                      color: Color.fromARGB(255, 145, 79, 156),
                      fontFamily: 'Poppins',
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromARGB(255, 145, 79, 156),
                      ),
                      borderRadius: new BorderRadius.circular(20.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromARGB(255, 145, 79, 156),
                      ),
                      borderRadius: new BorderRadius.circular(20.0),
                    ),
                  ),
                  validator: (value) {
                      if (value!.isEmpty) {
                        return 'kata sandi harus diisi';
                      }
                      return null;
                    },
                ),
                new Padding(
                  padding: new EdgeInsets.only(top: 20.0),
                ),


                TextFormField(
                  controller: controllerKonfirmasi,
                  obscureText: true,
                  cursorColor: Color.fromARGB(255, 145, 79, 156),
                  decoration: new InputDecoration(
                    hintText: "masukkan konfirmasi kata sandi",
                    hintStyle: TextStyle(
                      color: Colors.black54,
                      fontFamily: 'Poppins',
                    ),
                    labelText: "Konfirmasi Kata Sandi Baru",
                    labelStyle: TextStyle(
                      color: Color.fromARGB(255, 145, 79, 156),
                      fontFamily: 'Poppins',
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromARGB(255, 145, 79, 156),
                      ),
                      borderRadius: new BorderRadius.circular(20.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromARGB(255, 145, 79, 156),
                      ),
                      borderRadius: new BorderRadius.circular(20.0),
                    ),
                  ),
                  validator: (value) {
                      if (value!.isEmpty) {
                        return 'kata sandi harus diisi';
                      }
                      return null;
                    },
                ),
                new Padding(
                  padding: new EdgeInsets.only(top: 100.0),
                ),
                new RaisedButton(
                      child: new Text("Save",
                      style: TextStyle(fontFamily: 'Poppins'),),
                      color: Color.fromARGB(255, 145, 79, 156),
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          kirimdata();
                        }
                      })
              ]
              )
            ],
          ),
          ),
        ),
      ],
    );
  }
}