import 'package:flutter/material.dart';
import 'Register.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'BS Access',
      theme: ThemeData(
        fontFamily: 'Poppins-Medium',
        primarySwatch: Colors.blue,
      ),
      home: const Register(),
    );
  }
}

//email : eve.holt@reqres.in
//pw : cityslicka